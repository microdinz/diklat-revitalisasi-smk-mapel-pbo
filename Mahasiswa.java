/**
 *
 * @author Dinz
 */
public class Mahasiswa extends Person implements Informasi{
    private int nim;
    private String kelas, jurusan;

    public Mahasiswa(int nim, String nama, String kelas, String jurusan) {
        this.nim = nim;
        this.nama = nama;
        this.kelas = kelas;
        this.jurusan = jurusan;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    @Override
    public void info() {
        System.out.format("%-15s", nim);
        System.out.format("%-30s", nama);
        System.out.format("%-15s", kelas);
        System.out.format("%-30s", jurusan);
    }
}