/**
 *
 * @author Dinz
 */
public class Dosen extends Person implements Informasi{
    private String kode, mapel;

    public Dosen(String kode, String nama, String mapel){
        this.kode = kode;
        this.nama = nama;
        this.mapel = mapel;
    }
    
    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }
      
    @Override
    public void info() {
        System.out.format("%-15s", kode);
        System.out.format("%-30s", nama);
        System.out.format("%-45s", mapel);
    }
}