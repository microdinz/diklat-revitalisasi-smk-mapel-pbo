/**
 *
 * @author Dinz
 */
public class Main {

    private BufferedReader buf;
    private Scanner sc;
    private ArrayList<Dosen> dosen;
    private ArrayList<Mahasiswa> mahasiswa;
    private ArrayList<Nilai> nilai;
    private ArrayList<Nilai> dataSort;

    public Main() {
        buf = new BufferedReader(new InputStreamReader(System.in));
        sc = new Scanner(System.in);
        dosen = new ArrayList<>();
        mahasiswa = new ArrayList<>();
        nilai = new ArrayList<>();
        dataSort = new ArrayList<>();
    }

    public static void main(String[] args) {
        new Main().tampilkanMenu();
    }

    public void tampilkanMenu() {
        sc = new Scanner(System.in);
        int menu = 0;

        while (menu != 5) {
            System.out.println("");
            System.out.println("PENILAIAN MAHASISWA");
            System.out.println("--------------------");
            System.out.println("1. Tambah Data Dosen");
            System.out.println("2. Tambah Data Mahasiswa");
            System.out.println("3. Tambah Nilai Mahasiswa");
            System.out.println("4. Tampilkan Ranking Mahasiswa");
            System.out.println("5. Keluar");
            System.out.println("--------------------");
            System.out.print("Menu [1-5] : ");
            try {
                menu = sc.nextInt();
            } catch (Exception e) {
                System.out.println("Silahkan pilih menu!");
                tampilkanMenu();
            }

            switch (menu) {
                case 1:
                    tambahDosen();
                    break;
                case 2:
                    tambahMahasiswa();
                    break;
                case 3:
                    tambahNilai();
                    break;
                case 4:
                    tampilkanRanking();
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid menu!");
                    break;
            }
        }
    }

    public void tambahMahasiswa() {
        int nim;
        String nama, kelas, jurusan;
        try {
            System.out.println("");
            System.out.println("TAMBAH DATA MAHASISWA");
            System.out.println("---------------------");
            System.out.print("NIM: ");
            nim = Integer.valueOf(buf.readLine());
            System.out.print("Nama Mahasiswa: ");
            nama = buf.readLine();
            System.out.print("Kelas: ");
            kelas = buf.readLine();
            System.out.print("Jurusan: ");
            jurusan = buf.readLine();

            mahasiswa.add(new Mahasiswa(nim, nama, kelas, jurusan));
            tampilkanMahasiswa();
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Invalid input! Silahkan ulangi...");
        }
    }

    public void tampilkanMahasiswa() {
        System.out.println("");
        System.out.println("DATA MAHASISWA");
        System.out.println("--------------");
        System.out.println("");
        System.out.format("%-5s", "#");
        System.out.format("%-15s", "NIM");
        System.out.format("%-30s", "NAMA");
        System.out.format("%-15s", "KELAS");
        System.out.format("%-30s", "JURUSAN");
        System.out.println("");
        System.out.println("----------------------------------------------------------------------");
        for (int i = 0; i < mahasiswa.size(); i++) {
            System.out.format("%-5s", i + 1);
            mahasiswa.get(i).info();
            System.out.println("");
        }
        System.out.println("----------------------------------------------------------------------");
    }

    public void tambahDosen() {
        String kode, nama, mapel;
        try {
            System.out.println("");
            System.out.println("TAMBAH DATA DOSEN");
            System.out.println("---------------------");
            System.out.print("Kode Dosen: ");
            kode = buf.readLine();
            System.out.print("Nama Dosen: ");
            nama = buf.readLine();
            System.out.print("Mapel: ");
            mapel = buf.readLine();

            dosen.add(new Dosen(kode, nama, mapel));
            tampilkanDosen();
        } catch (IOException ex) {
            System.out.println("Invalid input! Silahkan ulangi...");
        }
    }

    public void tampilkanDosen() {
        System.out.println("");
        System.out.println("DATA DOSEN");
        System.out.println("----------");
        System.out.println("");
        System.out.format("%-5s", "#");
        System.out.format("%-15s", "KODE DOSEN");
        System.out.format("%-30s", "NAMA DOSEN");
        System.out.format("%-45s", "MAPEL");
        System.out.println("");
        System.out.println("----------------------------------------------------------------------");
        for (int i = 0; i < dosen.size(); i++) {
            System.out.format("%-5s", i + 1);
            dosen.get(i).info();
            System.out.println("");
        }
        System.out.println("----------------------------------------------------------------------");
    }

    public void tambahNilai() {
        String kode;
        int nim, uts, uas;

        tampilkanDosen();
        tampilkanMahasiswa();

        try {
            System.out.println("");
            System.out.println("TAMBAH DATA NILAI");
            System.out.println("---------------------");
            System.out.print("Kode Dosen: ");
            kode = buf.readLine();
            System.out.print("NIM: ");
            nim = Integer.valueOf(buf.readLine());
            System.out.print("Nilai UTS: ");
            uts = Integer.valueOf(buf.readLine());
            System.out.print("Nilai UAS: ");
            uas = Integer.valueOf(buf.readLine());

            nilai.add(new Nilai(kode, nim, uts, uas));
        } catch (IOException ex) {
            System.out.println("Invalid input! Silahkan ulangi...");
        }
    }

    public void tampilkanRanking() {
        String kode;
        float r, rKelas = 0;

        tampilkanDosen();
        try {
            System.out.println("");
            System.out.println("DATA RANKING");
            System.out.println("------------");
            System.out.println("");
            System.out.print("Pilih kode dosen: ");
            kode = buf.readLine();
            
            int indexDosen = getIndexDosen(dosen, kode);

            if (indexDosen == -1) {
                System.out.println("Maaf, kode dosen salah");
                return;
            }
            initRanking(kode);
            
            System.out.println("Kode Dosen  :"+ dosen.get(indexDosen).getKode());
            System.out.println("Nama Dosen  :"+ dosen.get(indexDosen).nama);
            System.out.println("Mapel       :"+ dosen.get(indexDosen).getMapel());
            System.out.println("");
            
            System.out.format("%-10s", "Rank");
            System.out.format("%-30s", "NAMA");
            System.out.format("%-10s", "UTS");
            System.out.format("%-10s", "UAS");
            System.out.format("%-20s", "Rata-rata");
            System.out.println("");
            System.out.println("----------------------------------------------------------------------");
            for (int i = 0; i < dataSort.size(); i++) {
                r = (float)(dataSort.get(i).getNilaiUTS() + dataSort.get(i).getNilaiUAS()) / 2;
                rKelas+=r;
                System.out.format("%-10s", i + 1);
                System.out.format("%-30s", getNamaMahasiswa(dataSort.get(i).getNim()));
                System.out.format("%-10s", dataSort.get(i).getNilaiUTS());
                System.out.format("%-10s", dataSort.get(i).getNilaiUAS());
                System.out.format("%-20s", r);
                System.out.println("");
            }
            System.out.println("----------------------------------------------------------------------");
            System.out.println("Rata - Rata Kelas : "+rKelas/dataSort.size());
        } catch (IOException ex) {
            System.out.println("Invalid input! Silahkan ulangi...");
        }
    }

    public int getIndexDosen(ArrayList<Dosen> data, String val) {
        int index = -1;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getKode().equals(val)) {
                index = i;
                break;
            }
        }
        return index;
    }
    
    public String getNamaMahasiswa(int val) {
        String nama = "-";
        for (int i = 0; i < mahasiswa.size(); i++) {
            if (mahasiswa.get(i).getNim() == val) {
                nama = mahasiswa.get(i).nama;
                break;
            }
        }
        return nama;
    }

    public void initRanking(String kode) {
        dataSort = new ArrayList<>();
        Nilai tmp;
        float n1, n2, r;

        for (int i = 0; i < nilai.size(); i++) {
            if (nilai.get(i).getKodeDosen().equals(kode)) {
                dataSort.add(nilai.get(i));
            }
        }

        for (int i = 1; i <= dataSort.size(); i++) {
            for (int k = i; k < dataSort.size(); k++) {
                n1 = (dataSort.get(i - 1).getNilaiUTS() + dataSort.get(i - 1).getNilaiUAS()) / 2;
                n2 = (dataSort.get(k).getNilaiUTS() + dataSort.get(k).getNilaiUAS()) / 2;
                if (n1 < n2) {
                    tmp = dataSort.get(k);
                    dataSort.remove(k);
                    dataSort.add(k, dataSort.get(i - 1));
                    dataSort.remove(i - 1);
                    dataSort.add(i - 1, tmp);
                }
            }
        }
    }
}